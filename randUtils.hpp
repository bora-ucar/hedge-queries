#ifndef RANDUTILS_HPP
#define RANDUTILS_HPP

#include <iostream>
#include <iomanip> 
#include <vector>
#include <cstdlib>
#include <cmath>
#include <string>


using namespace std;

void createQueriesHit(vector <vector<unsigned int>> const &hedges, unsigned int const numQueries, vector <vector<unsigned int>> &queries);
void createQueries(vector<unsigned int> const &dimensions, unsigned int const d, unsigned int const numQueries, vector <vector<unsigned int>> & queries);
void createQueriesMix(vector <vector<unsigned int>> const &hedges, vector<unsigned int> const &dimensions, unsigned int const d, unsigned int const numQueries, double &hitRatio, vector <vector<unsigned int>> & queries);
void setHyperedges(vector <vector<unsigned int>> &hedges, unsigned int &N, unsigned int d, vector<unsigned int> &dimensions);
void setDimensions(vector <unsigned int> &dimensions, unsigned int d, unsigned int minSz, unsigned int maxSz);
void setDimensions(unsigned int *dimensions, unsigned int d, unsigned int minSz, unsigned int maxSz);
void setHyperedges(unsigned int *hedges, unsigned int &N, unsigned int d, unsigned int *dimensions);


void reportTime(double times[][2], unsigned int numTests, string myMsg);
void randPermuteHedges(vector <vector<unsigned int>> &hedges, unsigned int N, unsigned int d);

void createQueriesMix(const unsigned int * hedges, const unsigned int *  dimensions, const unsigned int n, const unsigned int d, const unsigned int  numQueries, const double hitRatio,  unsigned int *queries);


#endif
