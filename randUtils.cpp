#include "randUtils.hpp"

void createQueriesHit(vector <vector<unsigned int>> const &hedges, unsigned int const numQueries, vector <vector<unsigned int>> & queries)
{
	queries.resize(numQueries);
	for (unsigned int j = 0; j < numQueries; j++)
	{
		unsigned int tg = rand() % hedges.size();
		queries[j] = hedges[tg];
	}
}
void createQueriesHit(const unsigned int * hedges, unsigned int n, unsigned int const d, unsigned int const numQueries, unsigned int *queries)
{
 	/* PRE: quearies allocated*/

	for (unsigned int j = 0; j < numQueries; j++)
	{
		unsigned int tg = rand() % n;
		for (unsigned int t = 0; t < d; t++)
			queries[j*d + t] = hedges[tg * d + t];
	}
}

void createQueries(vector<unsigned int> const &dimensions, unsigned int const d, unsigned int const numQueries, vector <vector<unsigned int>> & queries)
{
	queries.resize(numQueries);
	for (unsigned int j = 0; j < numQueries; j++)
	{
		queries[j].resize(d);
		for (unsigned int k = 0; k < d; k++)
		{
   		queries[j][k]=rand()%dimensions[k]; 
   	}
   }
}
void createQueries(const unsigned int *  dimensions, const unsigned int  d, const unsigned int  numQueries, unsigned int *queries)
{
	for (unsigned int j = 0; j < numQueries; j++)
	{
		for (unsigned int t = 0; t < d; t++)
		{
   		queries[j*d+t]=rand()%dimensions[t]; 
   	}
   }
}

void createQueriesMix(vector <vector<unsigned int>> const &hedges, vector<unsigned int> const &dimensions, unsigned int const d, unsigned int const numQueries, double &hitRatio, vector <vector<unsigned int>> & queries)
{	
	vector<vector<unsigned int>> qhits, qothers;
	unsigned int nqhit = (unsigned int) floor (hitRatio * numQueries);
	createQueriesHit(hedges, nqhit, qhits);

	for (unsigned int i = 0; i < nqhit; i++)
		queries.push_back(qhits[i]);

	unsigned int nnohits = numQueries - nqhit;

	if(nnohits >= 1)	
		createQueries(dimensions, d, nnohits, qothers);

	for (unsigned int i = 0; i < nnohits; i++)
		queries.push_back(qothers[i]);

	/*do an in-place shuffle to avoid any bias*/
	for (unsigned int i = 0; i < numQueries - 1; i++)
	{
		unsigned int tg = rand() % (numQueries - i) + i;
		vector<unsigned int> tmp(queries[i]);
		queries[i] = queries[tg];
		queries[tg] = tmp;
	}
}

void createQueriesMix(const unsigned int * hedges, const unsigned int *  dimensions, const unsigned int n, const unsigned int d, const unsigned int  numQueries, const double hitRatio,  unsigned int *queries)
{
	/* PRE: quearies allocated*/
	unsigned int nqhit = (unsigned int) floor (hitRatio * numQueries);
	createQueriesHit(hedges, n, d,  numQueries, queries);

	unsigned int nnohits = numQueries - nqhit;
	createQueries(dimensions, d, nnohits, &(queries[nqhit]));

	for (unsigned int i = 0; i < numQueries - 1; i++)
	{
		unsigned int tg = rand() % (numQueries - i) + i;
		for (unsigned int t = 0; t < d; t++)
		{
			unsigned int tmp = queries[i * d + t];
			queries[i * d + t] = queries[tg * d + t];
			queries[tg * d + t] = tmp;

		}

	}

}


void createQueries_gen(unsigned int const &numVtx, unsigned int const dmax, unsigned int const numQueries, vector <vector<unsigned int>> & queries)
{
	queries.resize(numQueries);
	vector <unsigned int> marker(numVtx);
	for (unsigned int j = 0; j < numQueries; j++)
	{
		unsigned int qsize = rand() % (dmax) ;
		if(qsize == 0) qsize++;
		qsize = qsize > 20 ? 20 : qsize; 

		queries[j].resize(qsize);
		unsigned int add=0;
		for (unsigned int k = 0; k < qsize; k++)
		{
			unsigned int v = rand()%numVtx+1;
			if(marker[v] != j+1)   			
			{
				marker[v] = j+1;
				add++;
   				queries[j].push_back(v); 
   			}
   		}
   		queries[j].resize(add);
   	}
   }

   void createQueriesMix_gen(vector <vector<unsigned int>> const &hedges, unsigned int const numVtx,  unsigned int const dmax, unsigned int const  numQueries, double &hitRatio, vector <vector<unsigned int>> & queries)
   {

   	vector<vector<unsigned int>> qhits, qothers;
   	unsigned int nqhit = (unsigned int) floor (hitRatio * numQueries);
   	createQueriesHit(hedges, nqhit, qhits);

   	for (unsigned int i = 0; i < nqhit; i++)			
   		queries.push_back(qhits[i]);

   	unsigned int nnohits = numQueries - nqhit;
   	if(nnohits >= 1)	
   		createQueries_gen(numVtx, dmax, nnohits, qothers);

   	for (unsigned int i = 0; i < nnohits; i++)
   		queries.push_back(qothers[i]);

   	vector <unsigned int> deleted;

	/*do an in-place shuffle to avoid any bias*/
   	for (unsigned int i = 0; i < numQueries - 1; i++)
   	{
   		unsigned int tg = rand() % (numQueries - i) + i;
   		vector<unsigned int> tmp(queries[i]);
   		queries[i] = queries[tg];
   		queries[tg] = tmp;
   	}
   }

   void setDimensions(vector <unsigned int> &dimensions, unsigned int d, unsigned int minSz, unsigned int maxSz)
   {
   	srand(time(NULL));
   	dimensions.resize(d);
   	for (unsigned int i = 0; i < d; i++)
		dimensions[i] = (rand()% (maxSz - minSz)) + minSz;/*betweenn minsz and maxSz - 1*/
   }


void setDimensions(unsigned int *dimensions, unsigned int d, unsigned int minSz, unsigned int maxSz)
{
	srand(time(NULL));
	for (unsigned int i = 0; i < d; i++)
		dimensions[i] = (rand()% (maxSz - minSz)) + minSz;/*betweenn minsz and maxSz - 1*/
}

unsigned int _global_d;
int comp_hedge_array(const void *h1,const void*h2)
{
	unsigned int *h1p = (unsigned int*) h1;
	unsigned int *h2p = (unsigned int*) h2;
	for (unsigned int i = 0; i < _global_d; i++)
	{
		if (h1p[i] < h2p[i]) return -1;
		if (h1p[i] > h2p[i]) return 1;
	}
	return 0;
}

void setHyperedges(unsigned int *hedges, unsigned int &N, unsigned int d, unsigned int *dimensions)
{
	vector <unsigned int> marker(N, 1);
	unsigned int j;

	createQueries(dimensions, d, N, hedges);

	_global_d = d;
	qsort(hedges, N, d * sizeof(unsigned int), comp_hedge_array) ;

	for(unsigned int i = 1; i < N; i++)
	{
		if (comp_hedge_array(&(hedges[(i-1)*d]) , &(hedges[i*d]))  == 0)
			marker[i] = 0;
	}

	j = 0;
	for (unsigned int i = 0; i < N; i++)
	{
		if(marker[i])
		{
			for (unsigned int t = 0; t < d; t++)
				hedges[j*d + t] = hedges[i*d + t];
			j++;
		}
	}
	N = j;
}

void reportTime(double times[][2], unsigned int numTests, string myMsg)
{
	double minT, maxT, avgT;

	for (int j=0; j < 2; j++)
	{
		minT = numeric_limits<double>::max(); maxT = -10.0; avgT = 0.0;

		for (unsigned int i = 0; i < numTests; i++)
		{
			avgT += times[i][j];
			if(minT > times[i][j]) minT = times[i][j];
			if(maxT < times[i][j]) maxT = times[i][j];
		}
		avgT /= numTests;
		if(j == 0)
			cout <<myMsg << "Cnstr ";
		else
			cout <<" Query ";

		cout << setprecision(3)<<fixed<<minT << " "<< setprecision(3)<<fixed<< avgT << " "<<setprecision(3)<< fixed<<maxT;
		if(j == 1)
			cout<<endl;
	}
}

void randPermuteHedges(vector <vector<unsigned int>> &hedges, unsigned int N, unsigned int d)
{
	for (unsigned int i = 0; i < N; i++)
	{
		unsigned int tg = rand() % (N - i) + i;
		vector<unsigned int> tmp(hedges[i]);
		hedges[i] = hedges[tg];
		hedges[tg] = tmp;
	}
}

