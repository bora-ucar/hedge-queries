#ifndef HASHALAFKS_H_
#define HASHALAFKS_H_

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

#include "extern/fastmod-master/include/fastmod.h"

void trouver_premier(const unsigned int m, unsigned int&p);

#define modpMersennehala(y, x)  \
{                                  \
	y = ((x) & (pp_hala_)) + ((x)>>(qq_hala_));\
	y = y >= (pp_hala_) ? y-= (pp_hala_) : y;  \
}

struct hashalaFKS
{
	hashalaFKS(unsigned int d, unsigned int N, vector<unsigned long long int> k, unsigned int pp_hala, unsigned int qq_hala) {d_ = d; N_ = N;  k_ = k; pp_hala_ = pp_hala; qq_hala_ = qq_hala; M_fastmod= fastmod::computeM_u32(N);}
	size_t operator()(const unsigned int * myKey) const noexcept
	{
		unsigned long long somme, aa ;
		unsigned int		indice;
		somme = 0;
		for (unsigned int j = 0; j < d_; j++)
			somme += myKey[j]*k_[j];
	modpMersennehala(aa, somme);
				indice = fastmod::fastmod_u32((unsigned int) aa, M_fastmod, N_);
		return  (size_t) indice;

	}
  bool operator()(const unsigned int *x, const unsigned int *y) const{
    for(unsigned int i=0;i<d_;i++) if (x[i] != y[i]) return false;
    return true;
  }
  

	uint64_t M_fastmod;// ,M_p_fastmod; 

	unsigned int d_, N_, pp_hala_, qq_hala_;
	vector<unsigned long long int> k_;
};

void findHashalaFKSparams(unsigned int *hedges,  unsigned int d, const unsigned int *dimensions,  unsigned int N, 
	 vector <unsigned long long int> &k, unsigned int &pp_hala, unsigned int &qq_hala);

void creation_hashalafks(unsigned int * hedges, const unsigned int N, 
	unordered_map<vector<unsigned int>, unsigned int, hashalaFKS, hashalaFKS>& myMap);

void testHashalaFKS(unsigned int *hedges,  unsigned int d,  unsigned int N, const unsigned int *dimensions, 
	unsigned int *queries,  unsigned int numQueries, double &cnstrTime, double &qryTime, vector<int> &resultsHashalaFKS);


#endif
