#!/bin/bash

# for plots in figures 7

    mtxfile="kmer_A2a"
    mtxOutFile="../outputs_data/"$mtxfile"_Figure7.out"
    tnsfile="nell-1"
    tnsOutFile="../outputs_data/"$tnsfile"_Figure7.out"


    echo "Hit-Ratio FKSLean (Ext Factor = 2.4) FKSLean (Ext Factor = 1.0)  FastFilter" > "../outputs_data/fks_vs_fastfilter_"$tnsfile".dat"
    
    echo "Hit-Ratio FKSLean (Ext Factor = 2.4) FKSLean (Ext Factor = 1.0)  FastFilter" > "../outputs_data/fks_vs_fastfilter_"$mtxfile".dat"

    for t in 0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1
    do
        ../exps_hedgeQueries_Figure7 -f $tnsfile".tns.bin" -q 10000000 -r 5 -b -t $t > "../outputs_data/"$tnsfile"_Figure7.out"


    dim=`tail -1 $tnsOutFile | cut -d ' ' -f 1`

    avgQueryTimeOffset=$((dim + 10))

    avgQueryTime_FKSlean=`tail -4 $tnsOutFile | head -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_FKSleanOne=`tail -4 $tnsOutFile | head -2 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_FastFilter=`tail -1 $tnsOutFile | cut -d ' ' -f $avgQueryTimeOffset`
    
    echo $t $avgQueryTime_FKSlean $avgQueryTime_FKSleanOne $avgQueryTime_FastFilter >> "../outputs_data/fks_vs_fastfilter_"$tnsfile".dat"


        ../exps_hedgeQueries_Figure7 -f $mtxfile".mtx.bin" -q 10000000 -r 5 -b -t $t > "../outputs_data/"$mtxfile"_Figure7.out"

    dim=`tail -1 $mtxOutFile | cut -d ' ' -f 1`

    avgQueryTimeOffset=$((dim + 10))

    avgQueryTime_FKSlean=`tail -4 $mtxOutFile | head -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_FKSleanOne=`tail -4 $mtxOutFile | head -2 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_FastFilter=`tail -1 $mtxOutFile | cut -d ' ' -f $avgQueryTimeOffset`
    
    echo $t $avgQueryTime_FKSlean $avgQueryTime_FKSleanOne $avgQueryTime_FastFilter >> "../outputs_data/fks_vs_fastfilter_"$mtxfile".dat"
    done

   gnuplot plot_fkslean_vs_fastfilters.plt
