#!/bin/bash

    LC_ALL=C.UTF-8

 FILES="
 ../outputs_data/kmer_A2a.out
 ../outputs_data/Queen_4147.out
 ../outputs_data/com-Orkut.out
 ../outputs_data/nell-1.out
 ../outputs_data/delicious-3d.out
 ../outputs_data/delicious-4d.out
 ../outputs_data/flickr-3d.out
 ../outputs_data/flickr-4d.out
 ../outputs_data/nell-2.out
 ../outputs_data/enron.out
 ../outputs_data/vast-2015-mc1-3d.out
 ../outputs_data/vast-2015-mc1-5d.out
 ../outputs_data/chicago-crime-comm.out
 ../outputs_data/uber.out
 ../outputs_data/lbnl-network.out
 "

echo "Name HashalaFKS(s) uRecSplit(8,100) uRecSplit(5,5) uBBH(1) uBBH(5) uPTHash(1) uPTHash(2) uPTHash(3) uPTHash(4) FKSlean" > ../outputs_data/table2.dat
echo "Name HashalaFKS(s) uRecSplit(8,100) uRecSplit(5,5) uBBH(1) uBBH(5) uPTHash(1) uPTHash(2) uPTHash(3) uPTHash(4) FKSlean" > ../outputs_data/table3.dat

geomeanConstructionTime_RecSplit=1.0
geomeanConstructionTime_RecSplit55=1.0
geomeanConstructionTime_BBH1=1.0
geomeanConstructionTime_BBH5=1.0
geomeanConstructionTime_PTHash1=1.0
geomeanConstructionTime_PTHash2=1.0
geomeanConstructionTime_PTHash3=1.0
geomeanConstructionTime_PTHash4=1.0
geomeanConstructionTime_FKSlean=1.0

geomeanQueryTime_RecSplit=1.0
geomeanQueryTime_RecSplit55=1.0
geomeanQueryTime_BBH1=1.0
geomeanQueryTime_BBH5=1.0
geomeanQueryTime_PTHash1=1.0
geomeanQueryTime_PTHash2=1.0
geomeanQueryTime_PTHash3=1.0
geomeanQueryTime_PTHash4=1.0
geomeanQueryTime_FKSlean=1.0

for OutFile in $FILES 
do
    dim=`tail -1 $OutFile | cut -d ' ' -f 1`
    avgConstructionTimeOffset=$((dim + 6))
    avgConstructionTime_Hashala=`tail -11 $OutFile | head -3 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_RecSplit=`tail -11 $OutFile | head -4 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_RecSplit55=`tail -11 $OutFile | head -5 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_BBH1=`tail -11 $OutFile | head -6 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_BBH5=`tail -11 $OutFile | head -7 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_PTHash1=`tail -11 $OutFile | head -8 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_PTHash2=`tail -11 $OutFile | head -9 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_PTHash3=`tail -11 $OutFile | head -10 | tail -1 | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_PTHash4=`tail -1 $OutFile | cut -d ' ' -f $avgConstructionTimeOffset`
    avgConstructionTime_FKSlean=`tail -11 $OutFile | head -1 | cut -d ' ' -f $avgConstructionTimeOffset`

   avgConstructionTime_RecSplit_normalized=`echo "scale=2; $avgConstructionTime_RecSplit/$avgConstructionTime_Hashala" | bc`
   avgConstructionTime_RecSplit55_normalized=`echo "scale=2; $avgConstructionTime_RecSplit55/$avgConstructionTime_Hashala" | bc`
   avgConstructionTime_BBH1_normalized=`echo "scale=2; $avgConstructionTime_BBH1/$avgConstructionTime_Hashala" | bc`
   avgConstructionTime_BBH5_normalized=`echo "scale=2; $avgConstructionTime_BBH5/$avgConstructionTime_Hashala" | bc`
   avgConstructionTime_PTHash1_normalized=`echo "scale=2; $avgConstructionTime_PTHash1/$avgConstructionTime_Hashala" | bc`
   avgConstructionTime_PTHash2_normalized=`echo "scale=2; $avgConstructionTime_PTHash2/$avgConstructionTime_Hashala" | bc`
   avgConstructionTime_PTHash3_normalized=`echo "scale=2; $avgConstructionTime_PTHash3/$avgConstructionTime_Hashala" | bc`
   avgConstructionTime_PTHash4_normalized=`echo "scale=2; $avgConstructionTime_PTHash4/$avgConstructionTime_Hashala" | bc`
   avgConstructionTime_FKSlean_normalized=`echo "scale=2; $avgConstructionTime_FKSlean/$avgConstructionTime_Hashala" | bc`


	geomeanConstructionTime_RecSplit=$( echo "scale=12; $geomeanConstructionTime_RecSplit * $avgConstructionTime_RecSplit_normalized" | bc )
	geomeanConstructionTime_RecSplit55=$( echo "scale=12; $geomeanConstructionTime_RecSplit55 * $avgConstructionTime_RecSplit55_normalized" | bc )
	geomeanConstructionTime_BBH1=$( echo "scale=12; $geomeanConstructionTime_BBH1 * $avgConstructionTime_BBH1_normalized" | bc )
	geomeanConstructionTime_BBH5=$( echo "scale=12; $geomeanConstructionTime_BBH5 * $avgConstructionTime_BBH5_normalized" | bc )
	geomeanConstructionTime_PTHash1=$( echo "scale=12; $geomeanConstructionTime_PTHash1 * $avgConstructionTime_PTHash1_normalized" | bc )
	geomeanConstructionTime_PTHash2=$( echo "scale=12; $geomeanConstructionTime_PTHash2 * $avgConstructionTime_PTHash2_normalized" | bc )
	geomeanConstructionTime_PTHash3=$( echo "scale=12; $geomeanConstructionTime_PTHash3 * $avgConstructionTime_PTHash3_normalized" | bc )
	geomeanConstructionTime_PTHash4=$( echo "scale=12; $geomeanConstructionTime_PTHash4 * $avgConstructionTime_PTHash4_normalized" | bc )
	geomeanConstructionTime_FKSlean=$( echo "scale=12; $geomeanConstructionTime_FKSlean * $avgConstructionTime_FKSlean_normalized" | bc )


    instanceName=`echo "$OutFile" | cut -d '/' -f 3 | cut -d '.' -f 1`

    echo $instanceName $avgConstructionTime_Hashala $avgConstructionTime_RecSplit_normalized $avgConstructionTime_RecSplit55_normalized $avgConstructionTime_BBH1_normalized $avgConstructionTime_BBH5_normalized $avgConstructionTime_PTHash1_normalized $avgConstructionTime_PTHash2_normalized $avgConstructionTime_PTHash3_normalized $avgConstructionTime_PTHash4_normalized $avgConstructionTime_FKSlean_normalized >> ../outputs_data/table2.dat 





    avgQueryTimeOffset=$((dim + 10))

    avgQueryTime_FKSlean=`tail -11 $OutFile | head -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_Hashala=`tail -11 $OutFile | head -3 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_RecSplit=`tail -11 $OutFile | head -4 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_RecSplit55=`tail -11 $OutFile | head -5 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_BBH1=`tail -11 $OutFile | head -6 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_BBH5=`tail -11 $OutFile | head -7 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_PTHash1=`tail -11 $OutFile | head -8 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_PTHash2=`tail -11 $OutFile | head -9 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_PTHash3=`tail -11 $OutFile | head -10 | tail -1 | cut -d ' ' -f $avgQueryTimeOffset`
    avgQueryTime_PTHash4=`tail -1 $OutFile | cut -d ' ' -f $avgQueryTimeOffset`


   avgQueryTime_RecSplit_normalized=`echo "scale=2; $avgQueryTime_RecSplit/$avgQueryTime_Hashala" | bc`
   avgQueryTime_RecSplit55_normalized=`echo "scale=2; $avgQueryTime_RecSplit55/$avgQueryTime_Hashala" | bc`
   avgQueryTime_BBH1_normalized=`echo "scale=2; $avgQueryTime_BBH1/$avgQueryTime_Hashala" | bc`
   avgQueryTime_BBH5_normalized=`echo "scale=2; $avgQueryTime_BBH5/$avgQueryTime_Hashala" | bc`
   avgQueryTime_PTHash1_normalized=`echo "scale=2; $avgQueryTime_PTHash1/$avgQueryTime_Hashala" | bc`
   avgQueryTime_PTHash2_normalized=`echo "scale=2; $avgQueryTime_PTHash2/$avgQueryTime_Hashala" | bc`
   avgQueryTime_PTHash3_normalized=`echo "scale=2; $avgQueryTime_PTHash3/$avgQueryTime_Hashala" | bc`
   avgQueryTime_PTHash4_normalized=`echo "scale=2; $avgQueryTime_PTHash4/$avgQueryTime_Hashala" | bc`
   avgQueryTime_FKSlean_normalized=`echo "scale=2; $avgQueryTime_FKSlean/$avgQueryTime_Hashala" | bc`


	geomeanQueryTime_RecSplit=$( echo "scale=12; $geomeanQueryTime_RecSplit * $avgQueryTime_RecSplit_normalized" | bc )
	geomeanQueryTime_RecSplit55=$( echo "scale=12; $geomeanQueryTime_RecSplit55 * $avgQueryTime_RecSplit55_normalized" | bc )
	geomeanQueryTime_BBH1=$( echo "scale=12; $geomeanQueryTime_BBH1 * $avgQueryTime_BBH1_normalized" | bc )
	geomeanQueryTime_BBH5=$( echo "scale=12; $geomeanQueryTime_BBH5 * $avgQueryTime_BBH5_normalized" | bc )
	geomeanQueryTime_PTHash1=$( echo "scale=12; $geomeanQueryTime_PTHash1 * $avgQueryTime_PTHash1_normalized" | bc )
	geomeanQueryTime_PTHash2=$( echo "scale=12; $geomeanQueryTime_PTHash2 * $avgQueryTime_PTHash2_normalized" | bc )
	geomeanQueryTime_PTHash3=$( echo "scale=12; $geomeanQueryTime_PTHash3 * $avgQueryTime_PTHash3_normalized" | bc )
	geomeanQueryTime_PTHash4=$( echo "scale=12; $geomeanQueryTime_PTHash4 * $avgQueryTime_PTHash4_normalized" | bc )
	geomeanQueryTime_FKSlean=$( echo "scale=12; $geomeanQueryTime_FKSlean * $avgQueryTime_FKSlean_normalized" | bc )

    echo $instanceName $avgQueryTime_Hashala $avgQueryTime_RecSplit_normalized $avgQueryTime_RecSplit55_normalized $avgQueryTime_BBH1_normalized $avgQueryTime_BBH5_normalized $avgQueryTime_PTHash1_normalized $avgQueryTime_PTHash2_normalized $avgQueryTime_PTHash3_normalized $avgQueryTime_PTHash4_normalized $avgQueryTime_FKSlean_normalized >> ../outputs_data/table3.dat 

done


	geomeanConstructionTime_RecSplit=$( echo "scale=12; e(1/15*l($geomeanConstructionTime_RecSplit))" | bc -l )
	geomeanConstructionTime_RecSplit55=$( echo "scale=12; e(1/15*l($geomeanConstructionTime_RecSplit55))" | bc -l )
	geomeanConstructionTime_BBH1=$( echo "scale=12; e(1/15*l($geomeanConstructionTime_BBH1))" | bc -l )
	geomeanConstructionTime_BBH5=$( echo "scale=12; e(1/15*l($geomeanConstructionTime_BBH5))" | bc -l )
	geomeanConstructionTime_PTHash1=$( echo "scale=12; e(1/15*l($geomeanConstructionTime_PTHash1))" | bc -l )
	geomeanConstructionTime_PTHash2=$( echo "scale=12; e(1/15*l($geomeanConstructionTime_PTHash2))" | bc -l )
	geomeanConstructionTime_PTHash3=$( echo "scale=12; e(1/15*l($geomeanConstructionTime_PTHash3))" | bc -l )
	geomeanConstructionTime_PTHash4=$( echo "scale=12; e(1/15*l($geomeanConstructionTime_PTHash4))" | bc -l )
	geomeanConstructionTime_FKSlean=$( echo "scale=12; e(1/15*l($geomeanConstructionTime_FKSlean))" | bc -l )



   printf "%s %s %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f\n" "geomean" "-" $geomeanConstructionTime_RecSplit $geomeanConstructionTime_RecSplit55 $geomeanConstructionTime_BBH1 $geomeanConstructionTime_BBH5 $geomeanConstructionTime_PTHash1 $geomeanConstructionTime_PTHash2 $geomeanConstructionTime_PTHash3 $geomeanConstructionTime_PTHash4 $geomeanConstructionTime_FKSlean >> ../outputs_data/table2.dat

	geomeanQueryTime_RecSplit=$( echo "scale=12; e(1/15*l($geomeanQueryTime_RecSplit))" | bc -l )
	geomeanQueryTime_RecSplit55=$( echo "scale=12; e(1/15*l($geomeanQueryTime_RecSplit55))" | bc -l )
	geomeanQueryTime_BBH1=$( echo "scale=12; e(1/15*l($geomeanQueryTime_BBH1))" | bc -l )
	geomeanQueryTime_BBH5=$( echo "scale=12; e(1/15*l($geomeanQueryTime_BBH5))" | bc  -l )
	geomeanQueryTime_PTHash1=$( echo "scale=12; e(1/15*l($geomeanQueryTime_PTHash1))" | bc -l )
	geomeanQueryTime_PTHash2=$( echo "scale=12; e(1/15*l($geomeanQueryTime_PTHash2))" | bc -l )
	geomeanQueryTime_PTHash3=$( echo "scale=12; e(1/15*l($geomeanQueryTime_PTHash3))" | bc -l )
	geomeanQueryTime_PTHash4=$( echo "scale=12; e(1/15*l($geomeanQueryTime_PTHash4))" | bc -l )
	geomeanQueryTime_FKSlean=$( echo "scale=12; e(1/15*l($geomeanQueryTime_FKSlean))" | bc -l )


  printf "%s %s %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f\n" "geomean" "-" $geomeanQueryTime_RecSplit $geomeanQueryTime_RecSplit55 $geomeanQueryTime_BBH1 $geomeanQueryTime_BBH5 $geomeanQueryTime_PTHash1 $geomeanQueryTime_PTHash2 $geomeanQueryTime_PTHash3 $geomeanQueryTime_PTHash4 $geomeanQueryTime_FKSlean >> ../outputs_data/table3.dat

###### displaying the output table-2 and table-3 ################

cat ../outputs_data/table2.dat  | column -t -s " " > ../outputs_data/table2.txt
cat ../outputs_data/table3.dat  | column -t -s " " > ../outputs_data/table3.txt
