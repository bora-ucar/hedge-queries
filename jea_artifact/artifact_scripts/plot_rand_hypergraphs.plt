set encoding utf8
set term pdf enhanced
set tics scale 1.5  # specify the size of the tics
set tics font ",16"
set key default
set key top left
set key at screen 0.1, 0.97 font ",11.75" vertical sample 5 spacing 1.1 width 1 height 1.5 maxrows 2

set xrange [3.5:16.5]
set xtics ("4" 4, "8" 8,"16" 16) nomirror
set yrange [3:14]
set ytics (4,6,8,10,12) nomirror
set tics out


############# plot for construction time ############
set output "../outputs_data/Figure6a.pdf"
set xlabel '{/Helvetica-Italic d}' font ",19.5";
set ylabel "Construction Time [s]" font ",19.5";
unset label
plot \
  '../outputs_data/construction-time-rand-hypergraph-n20million-t0.5.dat' using 1:4 title "HashàlaFKS" linetype 3 lc rgb '#008040' lw 2 ps 1 pt 3 with linespoints, \
  '' using 1:5 title "uPTHash(2)" linetype 9 dt 2 lc rgb '#9400D3' lw 2 ps 1 pt 9 with linespoints, \
  '' using 1:3 title "uRecSplit(5,5)" linetype 5 lc rgb 'black' lw 2 ps 1 pt 5 with linespoints, \
  '' using 1:6 title "uBBH(5)" linetype 7 dt 2 lc rgb 'blue' lw 2 ps 1 pt 7 with linespoints, \
  '' using 1:2 title "uPTHash(4)" linetype 1 lc rgb 'red' lw 2 ps 1 pt 1 with linespoints, \
  '' using 1:7 title "FKSlean" linetype 8 dt 2 lc rgb 'brown' lw 2 ps 1 pt 8 with linespoints, \


############# plot for query time ############
set output "../outputs_data/Figure6b.pdf"
set xlabel '{/Helvetica-Italic d}' font ",19.5";
set ylabel "Query Response Time [s]" font ",19.5";
set yrange [1:12]
set ytics (0,2,4,6,8,10) nomirror
unset label
plot \
  '../outputs_data/query-time-rand-hypergraph-n20million-t0.5.dat' using 1:2 title "HashàlaFKS" linetype 3 lc rgb '#008040' lw 2 ps 1 pt 3 with linespoints, \
  '' using 1:4 title "uBBH(5)" linetype 7 dt 2 lc rgb 'blue' lw 2 ps 1 pt 7 with linespoints, \
  '' using 1:3 title "uRecSplit(5,5)" linetype 5 lc rgb 'black' lw 2 ps 1 pt 5 with linespoints, \
  '' using 1:6 title "uPTHash(4)" linetype 1 lc rgb 'red' lw 2 ps 1 pt 1 with linespoints, \
  '' using 1:5 title "uPTHash(2)" linetype 9 dt 2 lc rgb '#9400D3' lw 2 ps 1 pt 9 with linespoints, \
  '' using 1:7 title "FKSlean" linetype 8 dt 2 lc rgb 'brown' lw 2 ps 1 pt 8 with linespoints, \
