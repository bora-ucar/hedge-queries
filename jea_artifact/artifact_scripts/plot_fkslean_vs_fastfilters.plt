set term pdf enhanced
set tics scale 1.5  # specify the size of the tics
set tics font ",16"
set key default
set key top left
set key at screen 0.035, 0.97 font ",12" vertical sample 4 spacing 1 width 1 height 1 maxrows 1

set xrange [-0.05:1.05]
set xtics (0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0) nomirror
#set yrange [0.5:3.25]
set yrange [0.5:*]
set ytics 0.5 nomirror
set tics out


############# plot for query time ############
set output "../outputs_data/Figure7a.pdf"
set xlabel '{hit-ratio}' font ",19.5";
set ylabel "Query Response Time [s]" font ",19.5";
unset label
plot \
  '../outputs_data/fks_vs_fastfilter_nell-1.dat' using 1:4 title "FastFilter" linetype 7 dt 2 lc rgb 'blue' lw 2 ps 1 pt 7 with linespoints, \
  '' using 1:3 title "FKSlean({/Symbol r}=1.0)" linetype 3 lc rgb '#008040' lw 2 ps 1 pt 3 with linespoints, \
  '' using 1:2 title "FKSlean({/Symbol r}=2.4)" linetype 8 dt 2 lc rgb 'brown' lw 2 ps 1 pt 8 with linespoints, \



set output "../outputs_data/Figure7b.pdf"
set xlabel '{hit-ratio}' font ",19.5";
set ylabel "Query Response Time [s]" font ",19.5";
unset label
plot \
  '../outputs_data/fks_vs_fastfilter_kmer_A2a.dat' using 1:4 title "FastFilter" linetype 7 dt 2 lc rgb 'blue' lw 2 ps 1 pt 7 with linespoints, \
  '' using 1:3 title "FKSlean({/Symbol r}=1.0)" linetype 3 lc rgb '#008040' lw 2 ps 1 pt 3 with linespoints, \
  '' using 1:2 title "FKSlean({/Symbol r}=2.4)" linetype 8 dt 2 lc rgb 'brown' lw 2 ps 1 pt 8 with linespoints, \
