#include<iostream>
#include<iomanip>
#include<string>
#include<vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <stdlib.h>
#include <ctime> 
#include <limits>
#include <unistd.h>
#include <cstdlib>
#include <getopt.h>
#include <unordered_map>


#include "fkslean.hpp"

#include "hashalaFKS.hpp"
#include "useRecSplit.hpp"
#include "randUtils.hpp"
#include "useBBH.hpp"
#include "usePTHash.hpp"

#include "testFastFilters.hpp"

using namespace std;
using namespace sux::function;


void printUsage(char *execName) 
{
	cout << "Usage : \n\t"<< execName << "\n"
	"\t--numHedges <z>  : the number of hyperedges\n"
	"\t--numDims <d>    : the number of dimensions\n"
	"\t--minSize <i>    : the minimum size in a dim\n"
	"\t--maxSize <x>    : the maximum size in a dim\n"	
	"\t--numQueries <q> : the number of queries\n"
	"\t[--numRuns <r>]  : the number of repetitions (optional, default 5)\n"
	"\t[--hitRatio <t>] : the ratio of hit queries (optional: defailt 0.5)\n"
	"\t--help <h>       : help\n";
}
int parseArguments(int argc, char *argv[],
	unsigned int &N, unsigned int &d, unsigned int & minSz, unsigned int & maxSz,
	unsigned int &numQueries, unsigned int &numTests, double &hitRatio)
{
	numTests = 5;
	numQueries = 0;
	N = d = minSz = maxSz = 0;
	hitRatio = 0.0;
	const char* const short_opts = "z:d:i:x:q:r:t:h";
	const option long_opts[] = {
		{"numHedges", required_argument, nullptr, 'z'},
		{"numDims", required_argument, nullptr, 'd'},
		{"minSize", required_argument, nullptr, 'i'},
		{"maxSize", required_argument, nullptr, 'x'},
		{"numQueries", required_argument, nullptr, 'q'},
		{"numRuns", optional_argument, nullptr, 'r'},
		{"hitRatio", optional_argument, nullptr, 't'},
		{"help", no_argument, nullptr, 'h'},
		{nullptr, no_argument, nullptr, 0}
	};

	while(1)
	{
		int c, option_index;
		c = getopt_long(argc, argv, short_opts,	long_opts, &option_index);
		if (c == -1)
			break;
		switch(c)
		{	
			case 'z' :
			N = (unsigned int) stoi(optarg);
			break;
			case 'd' :
			d = (unsigned int) stoi(optarg);
			break;
			case 'i' :
			minSz = (unsigned int) stoi(optarg);
			break;
			case 'x' :
			maxSz = (unsigned int) stoi(optarg);
			break;
			case 'q' :			
			numQueries = (unsigned int) stoi(optarg);
			break;
			case 'r' :
			numTests = (unsigned int) stoi(optarg);
			break;
			case 't' :
			hitRatio = (double) stof(optarg);
			break;
			case 'h' :
			case '?' :
			default:
			printUsage(argv[0]);
			return 1;
		}	
	}
	if(numQueries ==0 || N<=1 || N > 2147483647 || maxSz <=1 || (minSz >= maxSz) || hitRatio < 0 || hitRatio > 1.0 || numTests < 1)
	{
		cout <<"sizes are not specified/OK "<<endl;
		printUsage(argv[0]);
		return 1;
	}
	return  0;

}

#define CHCK_RES  213

int main(int argc, char* argv[])
{
	unsigned int numQueries;
	unsigned * hedges_array; 
	unsigned * dimensions_array;

	
	unsigned int numTests ;	
	unsigned int d, N,  minSz, maxSz;

	string msg, dimMsg;
	double hitRatio ;
	if (parseArguments(argc, argv, N, d, minSz, maxSz, numQueries, numTests, hitRatio))
	{
		cerr<<argv[0]<<": input cannot be parsed correctly\n"<<endl;
		return 1;
	}

	dimensions_array = (unsigned int*) malloc (sizeof(unsigned int) * d);
	hedges_array = (unsigned int *) malloc(sizeof(unsigned int) * d* (unsigned long long) N);

	setDimensions(dimensions_array, d, minSz, maxSz);
	setHyperedges(hedges_array, N, d, dimensions_array);

	unsigned int * queries_array = (unsigned int *) malloc (sizeof(unsigned int) * numQueries * d); // rows = N; cols = d;
	createQueriesMix(hedges_array, dimensions_array,  N, d, numQueries, hitRatio, queries_array);

/*
queries  = hedges;
numQueries = N;
randPermuteHedges(queries, N, d);
*/
	vector<int> resultsRdx(numQueries);
	vector<int> resultsFKSlean(numQueries);
	vector<int> resultsHashalaFKS(numQueries);
	vector<int> resultsRecSplit(numQueries);
	vector<int> resultsRecSplit55(numQueries);

	vector<int> resultsBBH1(numQueries);
	vector<int> resultsBBH5(numQueries);

	vector<int> resultsPTHash1(numQueries);
	vector<int> resultsPTHash2(numQueries);
	vector<int> resultsPTHash3(numQueries);
	vector<int> resultsPTHash4(numQueries);

	vector<int> resultsFilter(numQueries);

	double execTimesRadix[numTests][2];
	double execTimesFKSlean[numTests][2];
	double execTimesHashalaFKS[numTests][2];
	double execTimesRecSplit[numTests][2];
	double execTimesRecSplit55[numTests][2];

	double execTimesBBH1[numTests][2];
	double execTimesBBH5[numTests][2];

	double execTimesPTHash1[numTests][2];
	double execTimesPTHash2[numTests][2];
	double execTimesPTHash3[numTests][2];
	double execTimesPTHash4[numTests][2];

	double execTimesFilter[numTests][2];


	for (unsigned int i = 0; i < numTests ;i++)
	{		 
/*	
	randPermuteHedges(hedges, N, d);//because tesetRadix sorts...

			cout << "Will run radix"<<endl;
			testRadix(hedges, d, N, dimensions, queries, numQueries, execTimesRadix[i][0], execTimesRadix[i][1], resultsRdx);
*/
		cout << "Will run fastFilter+FKSpos"<<endl;					
		testFilter(hedges_array,  d,  N,   dimensions_array,  queries_array,  numQueries, execTimesFilter[i][0], execTimesFilter[i][1], resultsFilter, useFksLean4pos);


		cout << "Will run FKSLean"<<endl;					
		testFKSleanExt(hedges_array, d,  N, dimensions_array, queries_array, numQueries, execTimesFKSlean[i][0], execTimesFKSlean[i][1], resultsFKSlean, 2.4);

		cout << "Will run hashala"<<endl;
		testHashalaFKS(hedges_array, d, N, dimensions_array, queries_array, numQueries, execTimesHashalaFKS[i][0], execTimesHashalaFKS[i][1], resultsHashalaFKS);

		cout << "Will run uRecSplit"<<endl;			
		testRecSplit(hedges_array, d,  N, queries_array, numQueries, execTimesRecSplit[i][0], execTimesRecSplit[i][1], resultsRecSplit);

		cout << "Will run uRecSplit-55"<<endl;			
		testRecSplit55(hedges_array, d,  N, queries_array, numQueries, execTimesRecSplit55[i][0], execTimesRecSplit55[i][1], resultsRecSplit55);

		cout << "Will run uBBH1"<<endl;			
		testBBH(1, hedges_array, d,  N, queries_array, numQueries, execTimesBBH1[i][0], execTimesBBH1[i][1], resultsBBH1);

		cout << "Will run uBBH5"<<endl;			
		testBBH(5, hedges_array, d,  N, queries_array, numQueries, execTimesBBH5[i][0], execTimesBBH5[i][1], resultsBBH5);

		cout << "Will run uPTHash1"<<endl;
		testPTHash(hedges_array,  d, N,  queries_array, numQueries, execTimesPTHash1[i][0], execTimesPTHash1[i][1], resultsPTHash1, 1);

		cout << "Will run uPTHash2"<<endl;
		testPTHash(hedges_array,  d, N, queries_array, numQueries, execTimesPTHash2[i][0], execTimesPTHash2[i][1], resultsPTHash2, 2);

		cout << "Will run uPTHash3"<<endl;
		testPTHash(hedges_array,  d, N, queries_array, numQueries, execTimesPTHash3[i][0], execTimesPTHash3[i][1], resultsPTHash3, 3);

		cout << "Will run uPTHash4"<<endl;
		testPTHash(hedges_array,  d, N, queries_array, numQueries, execTimesPTHash4[i][0], execTimesPTHash4[i][1], resultsPTHash4, 4);

#ifdef CHCK_RES
		for (unsigned int jj = 0; jj < numQueries; jj++)
		{
			if(resultsFKSlean[jj] != resultsFilter[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsFilter[jj]<< " at " << jj<< " for FKS vs resultsFilter "<< endl;
				exit(25);
			}
			
			if(resultsFKSlean[jj] != resultsRecSplit[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsRecSplit[jj]<< " at " << jj<< " for FKS vs RecSplit "<< endl;
				exit(25);
			}

			if(resultsFKSlean[jj] != resultsHashalaFKS[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsHashalaFKS[jj]<< " at " << jj<< " for FKS vs hashalaFKS "<< endl;
				exit(25);
			}
			if(resultsFKSlean[jj] != resultsRecSplit55[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsRecSplit55[jj]<< " at " << jj<< " for FKS vs RecSplit55 "<< endl;
				exit(25);
			}

			if(resultsFKSlean[jj] != resultsBBH1[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsBBH1[jj]<< " at " << jj<< " for FKS vs BBH1 "<< endl;
				exit(25);
			}
			if(resultsFKSlean[jj] != resultsBBH5[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsBBH5[jj]<< " at " << jj<< " for FKS vs BBH5 "<< endl;
				exit(25);
			}

			if(resultsFKSlean[jj] != resultsPTHash1[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsPTHash1[jj]<< " at " << jj<< " for FKS vs PTHash1 "<< endl;
				exit(25);
			}

			if(resultsFKSlean[jj] != resultsPTHash2[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsPTHash2[jj]<< " at " << jj<< " for FKS vs PTHash2 "<< endl;
				exit(25);
			}

			if(resultsFKSlean[jj] != resultsPTHash3[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsPTHash3[jj]<< " at " << jj<< " for FKS vs PTHash3 "<< endl;
				exit(25);
			}

			if(resultsFKSlean[jj] != resultsPTHash4[jj])
			{
				cerr << resultsFKSlean[jj] << " vs " << resultsPTHash4[jj]<< " at " << jj<< " for FKS vs PTHash4 "<< endl;
				exit(25);
			}
		}	
#endif
	}

	dimMsg = to_string(d) + " ";
	for (unsigned int i = 0; i < d ; i++)
		dimMsg  += to_string(dimensions_array[i]) + " " ;
/*
		msg = dimMsg + " "+ to_string(N) +  "  RadixSortTimings ";
		reportTime(execTimesRadix, numTests, msg);
*/
	msg = dimMsg + " "+ to_string(N) +  "    FKSleanTimings ";
	reportTime(execTimesFKSlean, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  "   RecSplitTimings ";
	reportTime(execTimesRecSplit, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  " HashalaFKSTimings ";
	reportTime(execTimesHashalaFKS, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  "   RecSplitTimings55 ";
	reportTime(execTimesRecSplit55, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  "   FastFilters ";
	reportTime(execTimesFilter, numTests, msg);


	msg = dimMsg + " "+ to_string(N) +  "   BBH1 ";
	reportTime(execTimesBBH1, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  "   BBH5 ";
	reportTime(execTimesBBH5, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  "   PTHash1 ";
	reportTime(execTimesPTHash1, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  "   PTHash2 ";
	reportTime(execTimesPTHash2, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  "   PTHash3 ";
	reportTime(execTimesPTHash3, numTests, msg);

	msg = dimMsg + " "+ to_string(N) +  "   PTHash4 ";
	reportTime(execTimesPTHash4, numTests, msg);

	free(queries_array);
	free(dimensions_array);
	free(hedges_array);

	return 0;
}
