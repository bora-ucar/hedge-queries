 #include <sstream>
#include <iomanip>
#include <cmath>
#include <iostream>
#include<vector>
#include <fstream>
#include <pthread.h>

using namespace std;
#include "useBBH.hpp"
#include "extern/sux-master/sux/support/SpookyV2.hpp"
#include <chrono> 

double creationBBH(const int gamma,  unsigned int * hedges, const unsigned int d,  const unsigned int N, boophf_t **bphf, vector<unsigned int>& stockage)
{
	vector<uint64_t> input_keys(N);

	chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();
	for(unsigned int i=0; i < N; i++)
	{
		input_keys[i] = SpookyHash::Hash64((char *)&hedges[i*d], (d* sizeof(unsigned int))/sizeof(char), 0) ;
	}

	*bphf = new boomphf::mphf<u_int64_t,hasher_t>(N,input_keys,1,gamma, false);

	for(unsigned int i=0; i < N; i++)
	{
		stockage[(*bphf)->lookup(input_keys[i])] = i;
	}

	chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
	uint64_t elapsed = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();

	return (double)(elapsed * 1.E-9 );
}

void testBBH(const int gamma,  unsigned int *hedges, const unsigned int d, const unsigned int N, 
 unsigned int *queries, const unsigned int numQueries, double &cnstrTime, double &qryTime, vector<int> &resultsBBH)
{

	boophf_t *bphf ;
	vector<unsigned int> stockage(N);

	cnstrTime = creationBBH(gamma,hedges, d, N, &bphf,  stockage);
	chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();
	for (unsigned int j = 0; j < numQueries; j++)
	{

     	uint64_t myq = SpookyHash::Hash64((char *) &queries[j*d], (d*sizeof(unsigned int))/sizeof(char), 0);
		uint64_t atValue = bphf->lookup(myq);

		if (atValue >= N)
		{
			resultsBBH[j] = 0;
			continue;
		}
		unsigned int indice = stockage[ atValue];

		resultsBBH[j] = 1;
		for(unsigned int tt = 0 ; tt < d; tt++)
		{
			if (queries[j*d + tt] != hedges[indice*d + tt])	
			{				
				resultsBBH[j] = 0;
				break;
			}
		}
	}
	chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
	uint64_t elapsed = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
	qryTime = (double)(elapsed * 1.E-9 );

	delete bphf;
}

