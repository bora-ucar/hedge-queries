#ifndef USE_RECSPLIT_HPP
#define USE_RECSPLIT_HPP

using namespace std;

#include<vector>
#include<cstdlib>
#include<iostream>

#include "test/xoroshiro128pp.hpp"
#include "sux/function/RecSplit.hpp"

using namespace sux::function;

string uintToStrL(const unsigned int myInt, const unsigned int lngth);

void testRecSplit( unsigned int *hedges,  unsigned int d,  unsigned int N, 
  unsigned int *queries,  unsigned int numQueries, double &cnstrTime, double &qryTime, vector<int> &resultsRecSplit);

void testRecSplit55( unsigned int *hedges,  unsigned int d,  unsigned int N, 
  unsigned int *queries,  unsigned int numQueries, double &cnstrTime, double &qryTime, vector<int> &resultsRecSplit);


#endif
